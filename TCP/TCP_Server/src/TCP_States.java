/**
 * Created by Neway on 21/07/2016.
 */
public class TCP_States {

    enum TCP_NSTATES {
        TCPS_CLOSED,
        TCPS_LISTEN,
        TCPS_SYN_SENT,
        TCPS_SYN_RECEIVED,
        TCPS_ESTABLISHED,
        TCPS_CLOSE_WAIT,
        TCPS_FIN_WAIT_1,
        TCPS_CLOSING,
        TCPS_LAST_ACK,
        TCPS_FIN_WAIT_2,
        TCPS_TIME_WAIT
    }

}
